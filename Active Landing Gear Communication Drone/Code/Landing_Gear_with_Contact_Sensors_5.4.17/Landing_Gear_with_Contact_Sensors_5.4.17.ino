#include <Servo.h> 
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = 1;
int unlock = 150;
int s1_offset = -15;
int s2_offset = 10;
int s3_offset = -5;
int s4_offset = 20;
  
void setup(){
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  pinMode(2,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  digitalWrite(2,HIGH);
  digitalWrite(4,HIGH);
  digitalWrite(5,HIGH);
  digitalWrite(6,HIGH);
  servo1.write(lock+s1_offset);
  servo2.write(lock+s2_offset);
  servo3.write(lock+s3_offset);
  servo4.write(lock+s4_offset);
  while (number_feet_down(2,4,5,6) > 0){
        delay(10);
  }
  
}

void loop(){
// If all contacting: lock all legs
      if (number_feet_down(2,4,5,6) == 4){
      servo1.write(lock+s1_offset);
      servo2.write(lock+s2_offset);
      servo3.write(lock+s3_offset);
      servo4.write(lock+s4_offset);
      delay(500);
      if (number_feet_down(2,4,5,6) > 0){
        servo1.detach();
        servo2.detach();
        servo3.detach();
        servo4.detach();
        while (number_feet_down(2,4,5,6) > 0){
        delay(10);
        }
      }
      
      servo1.attach(7);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
      }  
      
// If 4 are not contacting: unlock all legs
      if (number_feet_down(2,4,5,6) == 0){
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
      }
}

//======================================FUNCTIONS===================================================================
//_____________________________________________________________________________________________

int foot_contact(int foot){
  //Reads foot sensor status. Returns 1 if contacting, 0 if not.
  if (digitalRead(foot) == LOW){
  return 1;}
  else{
    return 0;}
}
//_____________________________________________________________________________________________
int number_feet_down(int L1,int L2,int L3,int L4){
  //Returns number of feet contacting the ground
  return foot_contact(L1)+foot_contact(L2)+foot_contact(L3)+foot_contact(L4);
}


