#include <Servo.h> 
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = 0;
int unlock = 150;
int s1_offset = 0;
int s2_offset = 0;
int s3_offset = 0;
int s4_offset = 0;
int timer = 0;
  
void setup(){
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  pinMode(2,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  digitalWrite(2,HIGH);
  digitalWrite(4,HIGH);
  digitalWrite(5,HIGH);
  digitalWrite(6,HIGH);
  servo1.write(unlock+s1_offset);
  servo2.write(unlock+s2_offset);
  servo3.write(unlock+s3_offset);
  servo4.write(unlock+s4_offset);
  
}

void loop(){
// If one foot bottoms out: lock all legs
  if (digitalRead(2)==LOW){
      servo2.write(lock+s2_offset);
      servo3.write(lock+s3_offset);
      servo4.write(lock+s4_offset);
      delay(1500);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
      while (digitalRead(2)==LOW){delay(10);}
      servo1.attach(7);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
      delay(500);
      }
  
    
  if (digitalRead(4)==LOW){
      servo1.write(lock+s1_offset);
      servo3.write(lock+s3_offset);
      servo4.write(lock+s4_offset);
      delay(1500);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
      while (digitalRead(4)==LOW){delay(10);}   
      servo1.attach(7);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
      delay(500);

      }
    
    if (digitalRead(5)==LOW){
      servo1.write(lock+s1_offset);
      servo2.write(lock+s2_offset);
      servo4.write(lock+s4_offset);
      delay(1500);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
      while (digitalRead(5)==LOW){delay(10);}
      servo1.attach(7);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
      delay(500);      
      }
    if (digitalRead(6)==LOW){
      servo1.write(lock+s1_offset);
      servo2.write(lock+s2_offset);
      servo3.write(lock+s3_offset);
      delay(1500);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
      while (digitalRead(6)==LOW){delay(10);}
      servo1.attach(7);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
      delay(500);      
      }
      
      
    }  
 
