#include <Servo.h> 

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = 0;
int unlock = 150;
int s1_offset = 0;
int s2_offset = 0;
int s3_offset = 0;
int s4_offset = 0;
int trigger = 0;
float timer = 0;
float unlock_wait_time = 2; //seconds
float timestep = 0.01; //seconds
float delay_time = timestep*1000;
int switch1 = 0;
int switch2 = 0;
int switch3 = 0;
int switch4 = 0;


  
void setup(){
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  
  pinMode(2,INPUT_PULLUP);
  pinMode(4,INPUT_PULLUP);
  pinMode(5,INPUT_PULLUP);
  pinMode(6,INPUT_PULLUP);

  attachInterrupt(2,isr1,LOW);
  attachInterrupt(4,isr2,LOW);
  attachInterrupt(5,isr3,LOW);
  attachInterrupt(6,isr4,LOW);
  
  servo1.write(unlock+s1_offset);
  servo2.write(unlock+s2_offset);
  servo3.write(unlock+s3_offset);
  servo4.write(unlock+s4_offset);
}

void loop(){
if (switch1 = 1){
  LOCK_EXCEPT_L1;
  switch1 = 0;

if (switch2 = 1){
  LOCK_EXCEPT_L2;
  switch2 = 0;

if (switch3 = 1){
  LOCK_EXCEPT_L3;
  switch3 = 0;

if (switch4 = 1){
  LOCK_EXCEPT_L4;
  switch4 = 0;  
  
}
void isr1(){
  switch1=1;
}

void isr2(){
  switch2=1;
}

void isr3(){
  switch3=1;
}

void isr4(){
  switch4=1;
}


void LOCK_EXCEPT_L1(){
    servo4.write(lock+s4_offset);
    servo2.write(lock+s2_offset);
    servo3.write(lock+s3_offset);
    detach_legs();
    trigger = 0;
    while (trigger == 0){
      timer = 0;
      while (timer < unlock_wait_time){
        if (digitalRead(2)==HIGH){
        delay(delay_time);
        timer += timestep;}
        else {timer = 0;}}
      trigger = 1;}
    unlock_legs();
}

void LOCK_EXCEPT_L2(){
    servo3.write(lock+s3_offset);
    servo1.write(lock+s1_offset);
    servo4.write(lock+s4_offset);
    detach_legs();
    trigger = 0;
    while (trigger == 0){
      timer = 0;
      while (timer < unlock_wait_time){
        if (digitalRead(4)==HIGH){
        delay(delay_time);
        timer += timestep;}
        else {timer = 0;}}
      trigger = 1;}
    unlock_legs();
}

void LOCK_EXCEPT_L3(){
    servo2.write(lock+s2_offset);
    servo1.write(lock+s1_offset);
    servo4.write(lock+s4_offset);
    detach_legs();
    trigger = 0;
    while (trigger == 0){
      timer = 0;
      while (timer < unlock_wait_time){
        if (digitalRead(5)==HIGH){
        delay(delay_time);
        timer += timestep;}
        else {timer = 0;}}
      trigger = 1;}
    unlock_legs();
}

void LOCK_EXCEPT_L4(){
    servo1.write(lock+s1_offset);
    servo2.write(lock+s2_offset);
    servo3.write(lock+s3_offset);
    detach_legs();
    trigger = 0;
    while (trigger == 0){
      timer = 0;
      while (timer < unlock_wait_time){
        if (digitalRead(6)==HIGH){
        delay(delay_time);
        timer += timestep;}
        else {timer = 0;}}
      trigger = 1;}
    unlock_legs();
}

void detach_legs(){
      delay(1000);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
}

void unlock_legs(){
          servo1.attach(7);
          servo2.attach(10);
          servo3.attach(11);
          servo4.attach(12);
          servo1.write(unlock+s1_offset);
          servo2.write(unlock+s2_offset);
          servo3.write(unlock+s3_offset);
          servo4.write(unlock+s4_offset);
          delay(400);      //Allows legs to fall so the switch isn't triggered too quickly. Prevents unwanted lockups.
}

