#include <Servo.h> 

Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = 0;
int unlock = 150;
int s1_offset = 0;
int s2_offset = 0;
int s3_offset = 0;
int s4_offset = 0;
int trigger = 0;
float timer = 0.0;
float unlock_wait_time = 2.0; //seconds
float timestep = 0.01; //seconds
float delay_time = timestep*1000;


  
void setup(){
  Serial.begin(9600);
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  
  pinMode(2,INPUT);
  pinMode(4,INPUT);
  pinMode(5,INPUT);
  pinMode(6,INPUT);
  digitalWrite(2,HIGH);
  digitalWrite(4,HIGH);
  digitalWrite(5,HIGH);
  digitalWrite(6,HIGH);
  

  servo1.write(unlock+s1_offset);
  servo2.write(unlock+s2_offset);
  servo3.write(unlock+s3_offset);
  servo4.write(unlock+s4_offset);
}

void loop(){
// If one foot bottoms out: lock all legs
while (digitalRead(A0) == HIGH){
    lock_all_legs();
}
unlock_legs();
while (digitalRead(A0) == LOW){      
          
         if (digitalRead(2)==LOW){
            servo4.write(lock+s4_offset);
            servo2.write(lock+s2_offset);
            servo3.write(lock+s3_offset);
            detach_legs();
            trigger = 0;
            while (trigger == 0){
              timer = 0;
              while (timer < unlock_wait_time){
                if (digitalRead(2)==HIGH){
                delay(delay_time);
                timer += timestep;}
                else {timer = 0;}}
              trigger = 1;}
            unlock_legs();
         }
    
      
         if (digitalRead(4)==LOW){
            servo3.write(lock+s3_offset);
            servo1.write(lock+s1_offset);
            servo4.write(lock+s4_offset);
            detach_legs();
            trigger = 0;
            while (trigger == 0){
              timer = 0;
              while (timer < unlock_wait_time){
                if (digitalRead(4)==HIGH){
                delay(delay_time);
                timer += timestep;}
                else {timer = 0;}}
              trigger = 1;}
            unlock_legs();
         }
      
         if (digitalRead(5)==LOW){
            servo2.write(lock+s2_offset);
            servo1.write(lock+s1_offset);
            servo4.write(lock+s4_offset);
            delay(1000);
            servo2.detach();
            servo1.detach();
            servo4.detach();
            trigger = 0;
            while (trigger == 0){
              timer = 0;
              while (timer < unlock_wait_time){
                
                  
                if (digitalRead(5)==HIGH){
                delay(delay_time);
                timer += timestep;}
                else {timer = 0;}}
              trigger = 1;}
            unlock_legs();
         }
  
  
         if (digitalRead(6)==LOW){
            servo1.write(lock+s1_offset);
            servo2.write(lock+s2_offset);
            servo3.write(lock+s3_offset);
            delay(1000);
            servo1.detach();
            servo2.detach();
            servo3.detach();
            
            trigger = 0;
            while (trigger == 0){
              timer = 0;
              
              
              
              while (timer < unlock_wait_time){
                  while (digitalRead(A0) == HIGH){
                      servo4.write(lock+s4_offset);
                      delay(200);
                      servo4.detach();
                      }
                  servo4.attach(12);
                  servo4.write(unlock+s4_offset);
                      
                  if (digitalRead(6)==HIGH){
                  delay(delay_time);
                  timer += timestep;}
                  else {timer = 0;}
                }
              trigger = 1;}
            unlock_legs();
         }

}
}

void detach_legs(){
      delay(1000);
      servo1.detach();
      servo2.detach();
      servo3.detach();
      servo4.detach();
}

void lock_all_legs(){
    servo1.write(lock+s1_offset);
    servo2.write(lock+s2_offset);
    servo3.write(lock+s3_offset);
    servo4.write(lock+s4_offset);
    detach_legs();
}
void unlock_legs(){
          servo1.attach(7);
          servo2.attach(10);
          servo3.attach(11);
          servo4.attach(12);
          servo1.write(unlock+s1_offset);
          servo2.write(unlock+s2_offset);
          servo3.write(unlock+s3_offset);
          servo4.write(unlock+s4_offset);
          delay(400);      //Allows legs to fall so the switch isn't triggered too quickly. Prevents unwanted lockups.
}

