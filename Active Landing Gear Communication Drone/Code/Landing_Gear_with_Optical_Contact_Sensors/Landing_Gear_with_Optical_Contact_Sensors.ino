#include <Servo.h> 
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = -50;
int unlock = 150;
int s1_offset = 0;
int s2_offset = 0;
int s3_offset = 0;
int s4_offset = 0;

int A0_threshold = 290;
int A1_threshold = 260;
int A2_threshold = 260;
int A7_threshold = 260;

int A0_contact = 1;
int A1_contact = 1;
int A2_contact = 1;
int A7_contact = 1;

int A0_1 = 0;
int A0_2 = 0;
int A0_3 = 0;
int A0_4 = 0;
int A0_5 = 0;
int A0_6 = 0;
int A0_7 = 0;
int A0_8 = 0;
int A0_9 = 0;
int A0_10 = 0;

int A0_AVG = 0;

int A1_1 = 0;
int A1_2 = 0;
int A1_3 = 0;
int A1_4 = 0;
int A1_5 = 0;
int A1_6 = 0;
int A1_7 = 0;
int A1_8 = 0;
int A1_9 = 0;
int A1_10 = 0;
int A1_AVG = 0;

int A2_1 = 0;
int A2_2 = 0;
int A2_3 = 0;
int A2_4 = 0;
int A2_5 = 0;
int A2_6 = 0;
int A2_7 = 0;
int A2_8 = 0;
int A2_9 = 0;
int A2_10 = 0;
int A2_AVG = 0;

int A7_1 = 0;
int A7_2 = 0;
int A7_3 = 0;
int A7_4 = 0;
int A7_5 = 0;
int A7_6 = 0;
int A7_7 = 0;
int A7_8 = 0;
int A7_9 = 0;
int A7_10 = 0;
int A7_AVG = 0;


void setup(){
  Serial.begin(115200);
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A7,INPUT);
  

  servo1.write(lock+s1_offset);
  servo2.write(lock+s2_offset);
  servo3.write(lock+s3_offset);
  servo4.write(lock+s4_offset);
}

void loop(){
// If all contacting: lock all legs
      if (number_feet_down() == 4){
      lock_legs();
      delay(500);
      if (number_feet_down() >= 2){
        servo1.detach();
        servo2.detach();
        servo3.detach();
        servo4.detach();
        while (number_feet_down() >= 2){
        delay(1);
        }
        servo1.attach(7);
        servo2.attach(10);
        servo3.attach(11);
        servo4.attach(12);
      }
      
      }  
      
// If 4 are not contacting: unlock all legs
      else {
      unlock_legs();
      }
}

//======================================FUNCTIONS===================================================================
//_____________________________________________________________________________________________

int number_feet_down(){

A0_1 = A0_2;
A0_2 = A0_3;
A0_3 = A0_4;
A0_4 = A0_5;
A0_5 = A0_6;
A0_6 = A0_7;
A0_7 = A0_8;
A0_8 = A0_9;
A0_9 = A0_10;
A0_10 = analogRead(A0);
A0_AVG = (A0_1+A0_2+A0_3+A0_4+A0_5+A0_6+A0_7+A0_8+A0_9+A0_10)/10;

A1_1 = A1_2;
A1_2 = A1_3;
A1_3 = A1_4;
A1_4 = A1_5;
A1_5 = A1_6;
A1_6 = A1_7;
A1_7 = A1_8;
A1_8 = A1_9;
A1_9 = A1_10;
A1_10 = analogRead(A1);
A1_AVG = (A1_1+A1_2+A1_3+A1_4+A1_5+A1_6+A1_7+A1_8+A1_9+A1_10)/10;


A2_1 = A2_2;
A2_2 = A2_3;
A2_3 = A2_4;
A2_4 = A2_5;
A2_5 = A2_6;
A2_6 = A2_7;
A2_7 = A2_8;
A2_8 = A2_9;
A2_9 = A2_10;
A2_10 = analogRead(A2);
A2_AVG = (A2_1+A2_2+A2_3+A2_4+A2_5+A2_6+A2_7+A2_8+A2_9+A2_10)/10;


A7_1 = A7_2;
A7_2 = A7_3;
A7_3 = A7_4;
A7_4 = A7_5;
A7_5 = A7_6;
A7_6 = A7_7;
A7_7 = A7_8;
A7_8 = A7_9;
A7_9 = A7_10;
A7_10 = analogRead(A7);
A7_AVG = (A7_1+A7_2+A7_3+A7_4+A7_5+A7_6+A7_7+A7_8+A7_9+A7_10)/10;

if (A0_AVG <= A0_threshold){A0_contact = 1;}
  else {A0_contact = 0;}
  
if (A1_AVG <= A1_threshold){A1_contact = 1;}
  else {A1_contact = 0;}  
  
if (A2_AVG <= A2_threshold){A2_contact = 1;}
  else {A2_contact = 0;}  
  
if (A7_AVG <= A7_threshold){A7_contact = 1;}
  else {A7_contact = 0;}
 
Serial.print(A0_AVG);
Serial.print(", ");
Serial.print(A1_AVG);
Serial.print(", ");
Serial.print(A2_AVG);
Serial.print(", ");
Serial.print(A7_AVG);
Serial.print(", ");

Serial.println(A0_contact + A1_contact + A2_contact + A7_contact);

return A0_contact + A1_contact + A2_contact + A7_contact;

}

   
void lock_legs(){
  servo1.write(lock+s1_offset);
  servo2.write(lock+s2_offset);
  servo3.write(lock+s3_offset);
  servo4.write(lock+s4_offset);
}

void unlock_legs(){
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);
}

