#include <Servo.h> 
Servo servo1;
Servo servo2;
Servo servo3;
Servo servo4;
int lock = 1;
int unlock = 150;
int s1_offset = -15;
int s2_offset = 10;
int s3_offset = -5;
int s4_offset = 20;

int history_array [5][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
int new_history_array [5][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};


void setup(){
  Serial.begin(115200);
  servo1.attach(7);
  servo2.attach(10);
  servo3.attach(11);
  servo4.attach(12);
  pinMode(A0,INPUT);
  pinMode(A1,INPUT);
  pinMode(A2,INPUT);
  pinMode(A7,INPUT);
}

void loop(){
              if (number_feet_down() == 4){
                  lock_legs();
                  delay(500);
                  if (number_feet_down() >= 2){
                    servo1.detach();
                    servo2.detach();
                    servo3.detach();
                    servo4.detach();
                    while (number_feet_down() >= 2){
                    delay(10);
                    }
                    servo1.attach(7);
                    servo2.attach(10);
                    servo3.attach(11);
                    servo4.attach(12);
                  }
      
                 }  
      
// If 4 are not contacting: unlock all legs
      if (number_feet_down() < 1){
      unlock_legs();
      }
}

//======================================FUNCTIONS===================================================================
//_____________________________________________________________________________________________
int number_feet_down(){

history_array[0][0] = history_array[1][0];
history_array[0][1] = history_array[1][1];
history_array[0][2] = history_array[1][2];
history_array[0][3] = history_array[1][3];    

history_array[1][0] = history_array[2][0];
history_array[1][1] = history_array[2][1];
history_array[1][2] = history_array[2][2];
history_array[1][3] = history_array[2][3];  
  
history_array[2][0] = history_array[3][0];
history_array[2][1] = history_array[3][1];
history_array[2][2] = history_array[3][2];
history_array[2][3] = history_array[3][3];  
  
history_array[3][0] = history_array[4][0];
history_array[3][1] = history_array[4][1];
history_array[3][2] = history_array[4][2];
history_array[3][3] = history_array[4][3];

history_array[4][0] = analogRead(A0);
history_array[4][1] = analogRead(A1);
history_array[4][2] = analogRead(A2);
history_array[4][3] = analogRead(A7);



Serial.print(history_array[0][0]);
Serial.print(',');
Serial.print(history_array[0][1]);
Serial.print(',');
Serial.print(history_array[0][2]);
Serial.print(',');
Serial.println(history_array[0][3]);

Serial.print(history_array[1][0]);
Serial.print(',');
Serial.print(history_array[1][1]);
Serial.print(',');
Serial.print(history_array[1][2]);
Serial.print(',');
Serial.println(history_array[1][3]);

Serial.print(history_array[2][0]);
Serial.print(',');
Serial.print(history_array[2][1]);
Serial.print(',');
Serial.print(history_array[2][2]);
Serial.print(',');
Serial.println(history_array[2][3]);

Serial.print(history_array[3][0]);
Serial.print(',');
Serial.print(history_array[3][1]);
Serial.print(',');
Serial.print(history_array[3][2]);
Serial.print(',');
Serial.println(history_array[3][3]);

Serial.print(history_array[4][0]);
Serial.print(',');
Serial.print(history_array[4][1]);
Serial.print(',');
Serial.print(history_array[4][2]);
Serial.print(',');
Serial.println(history_array[4][3]);
Serial.println("");

delay(1000);
}
//_____________________________________________________________________________________________
void lock_legs(){
      servo1.write(lock+s1_offset);
      servo2.write(lock+s2_offset);
      servo3.write(lock+s3_offset);
      servo4.write(lock+s4_offset);}
//_____________________________________________________________________________________________
void unlock_legs(){
      servo1.write(unlock+s1_offset);
      servo2.write(unlock+s2_offset);
      servo3.write(unlock+s3_offset);
      servo4.write(unlock+s4_offset);}
//____________________________________________________________________________________________
