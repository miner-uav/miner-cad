    #include <Servo.h> 
    unsigned long pulse_width;
    Servo servo1;
    Servo servo2;
    Servo servo3;
    Servo servo4;
    int lock = 30;
    int unlock = 125;
    unsigned long p1 = 0;
    unsigned long p2 = 0;
    unsigned long p3 = 0;
    unsigned long p4 = 0;
    unsigned long p5 = 0;
    unsigned long p6 = 0;
    unsigned long p7 = 0;
    unsigned long p8 = 0;
    unsigned long p9 = 0;    
    unsigned long p10 = 0;    
    unsigned long p_AVG = 0;
    
    void setup()
    {
      //Serial.begin(115200); // Start serial communications
      servo1.attach(9);
      servo2.attach(10);
      servo3.attach(11);
      servo4.attach(12);
    
      pinMode(2, OUTPUT); // Set pin 2 as trigger pin
      pinMode(4, INPUT); // Set pin 3 as monitor pin
      digitalWrite(2, LOW); // Set trigger LOW for continuous read
    }
    
    void loop()
    {
      p1 = p2;
      p2 = p3;
      p3 = p4;
      p4 = p5;
      p5 = p6;
      p6 = p7;
      p7 = p8;
      p8 = p9;
      p9 = p10;      
      p10 = pulseIn(4, HIGH)/10;
      p_AVG = (p1+p2+p3+p4+p5+p6+p7+p8+p9+p10)/10.0;
      
      if(p_AVG <= 5){servo1.write(lock);
                      servo2.write(lock);
                      servo3.write(lock);
                      servo4.write(lock);}
      
      if(p_AVG > 5){servo1.write(unlock);
                     servo2.write(unlock);
                     servo3.write(unlock);
                     servo4.write(unlock);}
    }

